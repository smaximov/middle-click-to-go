/**
 * Copyright (C) 2016 smaximov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const gulp = require('gulp')
const remoteSrc = require('gulp-remote-src')
const conflict = require('gulp-conflict')
const rename = require('gulp-rename')

gulp.task('tld:fetch', () => {
  const remoteOptions = {
    base: 'https://tld-list.com/df/'
  }
  return remoteSrc('tld-list-basic.json', remoteOptions)
    .pipe(rename('tld-list.json'))
    .pipe(conflict('data/'))
    .pipe(gulp.dest('data/'))
})
