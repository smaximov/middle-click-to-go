/**
 * Copyright (C) 2016 smaximov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

class Fetch {
  // Convert a path to a chrome extension's file to an absolute URL
  static url(path) {
    return chrome.extension.getURL(path)
  }

  // Fetch a chrome extension's file
  static fetch(path) {
    return fetch(Fetch.url(path))
  }

  static json(path) {
    return Fetch.fetch(`data/${path}.json`)
      .then(response => response.json())
  }
}

const DOMAINS = Fetch.json('tld-list').then(json => new Set(json))

// Element to ignore paste event
const INPUTS = new Set([
  "textarea", "text", "password", "date",
  "email", "number", "range", "search", "tel",
  "time", "url", "week", "month"
])

// Regex to match a valid URL with optional scheme (protocol identifier).
//
// Captures protocol (scheme) and TLD  (top-level domain) identifiers.
// Taken from https://gist.github.com/dperini/729294.
const URL_REGEX = new RegExp(
  "^" +
    // OPTIONAL protocol identifier - CAPTURE(1)
    "((?:https?|ftp)://)?" +
    // user:pass authentication
    "(?:\\S+(?::\\S*)?@)?" +
    "(?:" +
      // IP address exclusion
      // private & local networks
      "(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
      "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
      "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})" +
      // IP address dotted notation octets
      // excludes loopback network 0.0.0.0
      // excludes reserved space >= 224.0.0.0
      // excludes network & broacast addresses
      // (first & last IP address of each class)
      "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
      "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
      "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
    "|" +
      // host name
      "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
      // domain name
      "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
      // TLD identifier - CAPTURE(2)
      "(?:\\.([a-z\\u00a1-\\uffff]{2,}))" +
      // TLD may end with dot
      "\\.?" +
    ")" +
    // port number
    "(?::\\d{2,5})?" +
    // resource path
    "(?:[/?#]\\S*)?" +
  "$", "i"
)

class Browser {
  // Shortcut to `chrome.runtime.sendMessage'
  static send(message, responseCallback) {
    return chrome.runtime.sendMessage(message, responseCallback)
  }

  // Navigate to a URL
  static navigate(url) {
    Browser.send({
      action: 'navigate',
      url: url
    })
  }

  // Perform search
  static search(query) {
    Browser.send({
      action: 'navigate',
      url: `https://google.com/search?q=${encodeURIComponent(query)}`
    })
  }
}

class ClipboardContent {
  constructor(data, type='text') {
    this._data = data
    this._type = type
  }

  get empty() {
    return this.data === undefined ||
      this.data === null ||
      this.data.length === 0
  }

  get data() {
    return this._data
  }

  get type() {
    return this._type
  }

  static text(text) {
    return new ClipboardContent(text, 'text')
  }

  static url(url) {
    return new ClipboardContent(url, 'url')
  }
}

class App {
  get inputs() {
    return INPUTS
  }

  get domains() {
    return DOMAINS
  }

  isInput(element) {
    return this.inputs.has(element.type)
  }

  getClipboardContent(event) {
    const query = event.clipboardData.getData('text/plain').trim()

    return this.domains.then(domains => {
      const captures = query.match(URL_REGEX)

      // URL-like thingy
      if (captures !== null) {
        const [, protocol, domain] = captures

        // Missing protocol identifier
        if (protocol === undefined) {
          // Check if `domain' is a known top-level domain
          // before prepending default protocol
          if (domains.has(domain)) {
            return ClipboardContent.url(`http://${query}`)
          }
          // If reached here, fallback to search
        } else {
          // Proper URL, no need to modify
          return ClipboardContent.url(query)
        }
      }

      // Not a URL, perform search
      return ClipboardContent.text(query)
    })
  }

  handle(event, handler) {
    document.addEventListener(event, handler, false)
  }

  run() {
    this.handle('paste', (event) => {
      const target = event.target
      const { isContentEditable } = target

      this.getClipboardContent(event).then(clipboardContent => {
        if (this.isInput(target) || isContentEditable || clipboardContent.empty) {
          return
        }

        if (clipboardContent.type === 'url') {
          Browser.navigate(clipboardContent.data)
        } else {
          Browser.search(clipboardContent.data)
        }
      })
    })
  }
}

// Fix opening links on the google search page in a background tab
if (document.body) {
  const app = new App()
  app.run()
}
