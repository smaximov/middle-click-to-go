<a name=""></a>
##  v0.4.0 (drop "scroll to switch tabs") (2016-04-18)


#### Regressions

*   drop "scroll to switch tab" feature ([5ea889a4](https://gitlab.com/smaximov/middle-click-to-go/commit/5ea889a499327789f96793fcdd42fc28aef6d637), closes [#10](https://gitlab.com/smaximov/middle-click-to-go/issues/10))

#### Features

*   basic support for i18n ([7a0d8519](https://gitlab.com/smaximov/middle-click-to-go/commit/7a0d8519592707ba73285e2b4e8ce2b3d7a01398))

#### Bug Fixes

*   paste on inputs shouldn't trigger search/navigate (fix #9) ([19a29019](https://gitlab.com/smaximov/middle-click-to-go/commit/19a290195fb0367042dedf93846fd982b63b363f))



<a name=""></a>
##  v0.3.0 (2016-03-16)


#### Features

*   hold down the right mouse button and scroll to switch tabs ([e93f5939](https://gitlab.com/smaximov/middle-click-to-go/commit/e93f59393bf3c311af5380a4c2de591715c5f610))
*   add a client API call to activate a tab ([8ca5c311](https://gitlab.com/smaximov/middle-click-to-go/commit/8ca5c3116dd2e7a27d25e69e8fd2b4090963369f))
*   add a client API call to query the current active tab ([359b0901](https://gitlab.com/smaximov/middle-click-to-go/commit/359b09018fc52967f039d6aab8c51e99e81ede41))
*   add a client API call to query a list of all tabs in the current window ([ae77025d](https://gitlab.com/smaximov/middle-click-to-go/commit/ae77025dc2da28e7e2d83ed58cc6a1ac54bc7cfd))

#### Bug Fixes

*   open links on the google search page in a background tab ([836caf04](https://gitlab.com/smaximov/middle-click-to-go/commit/836caf04be28d630005e06e7b9b566441a5b1d04))



<a name=""></a>
##  v0.2.0 (2016-03-12)


#### Bug Fixes

*   check for top-level domain to recognize an URL ([a6fcc2d3](https://gitlab.com/smaximov/middle-click-to-go/commit/a6fcc2d3a76ba34099a16ecbbcf6b4a56aac59bd))
*   paste URLs without scheme ([512fb1a5](https://gitlab.com/smaximov/middle-click-to-go/commit/512fb1a5db73bae31495fd080c1272dbda0875cc))
*   paste on elements containing anchors ([703f096f](https://gitlab.com/smaximov/middle-click-to-go/commit/703f096fc099dc622d9aada80d2563c0ec9458cc))

#### Features

*   pasting non-URL string starts search ([719d50a6](https://gitlab.com/smaximov/middle-click-to-go/commit/719d50a69a2256a4a7301553760a39c5aabdf945))



<a name=""></a>
##  v0.1.0: Initial version (2016-03-12)


#### Features

*   implement navigate on paste with middle click ([699eb322](https://gitlab.com/smaximov/middle-click-to-go/commit/699eb322bc91dc81c84981f7cced8cac53e9edd5))



